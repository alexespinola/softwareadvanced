<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('nombre');
            $table->string('apellido');
            $table->string('dni');
            $table->date('fecha_nacimiento');
            $table->string('trabaja_en')->nullable();
            $table->string('direccion')->nullable();
            $table->string('direccion_laboral')->nullable();
            $table->string('tel_fijo')->nullable();
            $table->string('tel_celular')->nullable();
            $table->string('tel_laboral')->nullable();
            $table->string('tel_ref1')->nullable();
            $table->string('tel_ref2')->nullable();
            $table->string('tel_ref3')->nullable();
            $table->string('tel_ref4')->nullable();
            $table->string('email')->nullable();
            $table->text('comentarios')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
