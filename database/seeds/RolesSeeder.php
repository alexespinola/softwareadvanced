<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       
    	\DB::table('roles')->delete();
        
        \DB::table('roles')->insert(array (
            0 => 
            array (
                'id' => 1,
                'nombre' => 'Administrador', 
            ),
            1 => 
            array (
                'id' => 2,
                'nombre' => 'Usuario Registrado', 
            ),
             array (
                'id' => 3,
                'nombre' => 'Vendedor', 
            ),
        ));
	    
    }
}
