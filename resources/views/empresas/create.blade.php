@extends('layouts.app')
@section('header_title', 'Alta de Empresas')
@section('header_subtitle', 'Inserta un registro en el Listado de mutuales, financieras etc.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/empresas')}}"><i class="fa fa-building"></i> Empresas</a></li>
    <li class="active"> <i class="fa fa-building"></i> Alta de empresas</li>
  </ol>
@endsection

@section('content')
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">
                
                {!! Form::open(['route' => 'empresas.store']) !!}

                    @include('empresas.fields')

                {!! Form::close() !!}
                
            </div>
        </div>
      </div>
    </div>
@endsection
