
<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{!! $empresa->nombre !!}</p>
</div>

<!-- Precio Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Dirección:') !!}
    <p>{!! $empresa->direccion !!}</p>
</div>

<!-- Rubro Id Field -->
<div class="form-group">
    {!! Form::label('telefonos', 'Teléfonos:') !!}
    <p>{!! $empresa->telefonos !!}</p>
</div>

<!-- Rubro Id Field -->
<div class="form-group">
    {!! Form::label('email', 'Correo electrónico:') !!}
    <p>{!! $empresa->email !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Creada el:') !!}
    <p>{!! $empresa->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Modificada el:') !!}
    <p>{!! $empresa->updated_at !!}</p>
</div>

