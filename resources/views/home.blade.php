@extends('layouts.app')
@section('header_title', 'Inicio')
@section('header_subtitle', 'Resumen')

@section('camino')
  <ol class="breadcrumb">
    {{-- <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li> --}}
    <li class="active"> <i class="fa fa-home"></i> Inicio</li>
  </ol>
@endsection

@section('content')

    <div class="row">

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$creditos_del_mes}}</h3>

              <p>Avisos</p>
            </div>
            <div class="icon">
              <i class="fa fa-bell"></i>
            </div>
            <a href="{{ asset('creditos') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$creditos_pendientes_de_cobro}}</h3>

              <p>Alertas</p>
            </div>
            <div class="icon">
              <i class="ion ion-alert-circled"></i>
            </div>
            <a href="{{ asset('creditos'.'?estado_id=3') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        

        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$creditos_cancelados}}</h3>

              <p>Tareas</p>
            </div>
            <div class="icon">
              <i class="fa fa-tasks"></i>
            </div>
            <a href="{{ asset('creditos'.'?estado_id=6') }}" class="small-box-footer">Ver detalle <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

    </div>

   
    <!-- AVISOS -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Avisos</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body">
        <p>Aquí se visualizarán los avisos que genere el sistema. Por ejemplo: cuando un cliente pago la ultima cuota o puede sacar una renovación</p>
      </div>
    </div>
     <!-- ALERTAS -->
    <div class="box box-warning">
      <div class="box-header with-border">
        <h3 class="box-title">Alertas</h3>
        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>
      </div>
      <div class="box-body">
        <p>Aquí se visualizarán las alertas previamente cargadas en el sistema. Por ejemplo: Fechas de cierre de liquidacion de cada prestadora</p>
      </div>
    </div>
     <!-- TAREAS -->
    <div class="box box-danger">
      <div class="box-header with-border">
        <h3 class="box-title">Tareas</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
          </button>
          {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
        </div>  
      </div>
      <div class="box-body" style="display: block;">
        <p>Aquí se visualizarán las tareas previamente cargadas en el sistema. Por ejemplo: "Cobrar cheque de Rodriguez por caja en el banco Nación"</p>
        Se mostrara el estado de cada tarea (pendiente, En espera, Realizada ) y a que usuario fue asignada.
      </div>
    </div>
  


@endsection


@section('scripts')


  <script>
    //-------------
    //- BAR CHART -
    //-------------
  window.onload = function() {


  }
  
  </script>

@endsection