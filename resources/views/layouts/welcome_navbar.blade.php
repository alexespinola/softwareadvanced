<link rel="stylesheet" type="text/css" href="{{ asset('css/welcome.css') }}">

<style type="text/css">
  
  @font-face {
    font-family: 'NEWTOW_B';
    src: url('fonts/NEWTOW_B.TTF');
  }


	
</style>


<nav class="navbar navbar-inverse navbar-fixed-top barra_menu">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a href="#">
      	<img src="{{ asset('img/logo-sisarg-transparente.png') }}" style="width: 250px; padding: 5px;  margin-right: 40px;">
    	</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
       
      </ul>
      
      <ul class="nav navbar-nav navbar-right">
        <li class="{{ isActiveRoute('/') }}"><a href="{{ url('/')}}"><b>SERVICIOS</b></a></li>
        <li class="{{ isActiveRoute('contacto') }}"><a href="{{ url('/contacto')}}"><b>CONTACTO</b></a></li>
        <li class="{{ isActiveRoute('consultas') }}"><a href="{{ url('/consultas')}}"><b>CONSULTAS</b></a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>