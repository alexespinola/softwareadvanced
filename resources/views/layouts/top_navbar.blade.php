<header class="main-header">
  <!-- Logo -->
  <a href="#" class="logo" style="text-align: left;">
    <!-- mini logo for sidebar mini 50x50 pixels -->
    <span class="logo-mini"><img src="{{ asset('img/sa.ico') }}" width="30" style="margin-left: 10px;"></span>
      {{-- <span class="logo-mini">&nbsp;&nbsp;<b>G</b>C</span> --}}
    <!-- logo for regular state and mobile devices -->
    <img src="{{ asset('img/logo5.png') }}"  alt="" style="width: 125px; margin-bottom: 4px;">
  </a>
  <!-- Header Navbar: style can be found in header.less -->
  <nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">
        <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">{{$consultas =  App\Consulta::count() }}</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Tenés {{$consultas}} consultas de la WEB</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  @foreach (App\Consulta::where('leida', 0)->orderBy('id', 'desc')->get() as $consulta)
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <i class="fa fa-info-circle"></i>
                      </div>
                      <h4>
                        {{$consulta->nombre}}
                        <?php $now = Carbon\Carbon::now(); ?>
                        <small><i class="fa fa-clock-o"></i> {{Carbon\Carbon::parse($consulta->created_at)->diffForHumans($now) }}</small>
                      </h4>
                      <p>{{$consulta->texto}}</p>
                    </a>
                  </li>
                  <!-- end message -->
                  @endforeach
                </ul>
              </li>
              <li class="footer"><a href="{{url('consultas_web')}}">Ver todas las consultas</a></li>
            </ul>
          </li>
        <!-- Notifications: style can be found in dropdown.less -->
          @if (!Auth::guest())
            <li class="dropdown notifications-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-bell-o"></i>
                <span class="label label-warning">{{$notificaciones =  App\Credito::where('fecha_ultima_cuota', date('Y-m-d'))->count() }}</span>
              </a>
              @if($notificaciones)
                <ul class="dropdown-menu">
                  <li class="header">Hoy vence la última cuota de {{$notificaciones}} créditos</li>
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      @foreach (App\Credito::where('fecha_ultima_cuota', date('Y-m-d'))->get() as $n)
                        <li>
                          <a href="#">
                            <i class="fa fa-warning text-yellow"></i> 
                            {{$n->cliente->nombre.' '.$n->cliente->apellido.' $'.$n->monto.' en '.$n->cuotas.' cuotas'  }}
                          </a>
                        </li>
                      @endforeach
                    </ul>
                  </li>
                  {{-- <li class="footer"><a href="#">View all</a></li> --}}
                </ul>
              @endif
            </li>
          @endif
        <!-- Tasks: style can be found in dropdown.less -->
          {{-- <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Create a nice theme
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Some task I need to do
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Make beautiful transitions
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li> --}}
        <!-- User Account: style can be found in dropdown.less -->
        <li class="dropdown user user-menu">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            @if (Auth::guest())
             {{--  <span class="hidden-xs">Usuario no identificado</span> --}}
            @else
            <img  src="{{ asset('admin_lte/dist/img/user.png') }}" width="50" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            @endif
            
          </a>
          @if (!Auth::guest())
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="{{ asset('admin_lte/dist/img/user.png') }}" class="img-circle" alt="User Image">

                <p>
                  {{ Auth::user()->name }}
                  <small>Regisdtrado el {{ date('d/m/y', strtotime(Auth::user()->created_at)) }}</small>
                </p>
              </li>
              <!-- Menu Body -->
              {{-- <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li> --}}
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Cerrar Sesión</a>

                </div>
              </li>
            </ul>
          @endif
        </li>
        <!-- Control Sidebar Toggle Button -->
        <!-- <li>
          <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
        </li> -->
      </ul>
    </div>
  </nav>
</header>