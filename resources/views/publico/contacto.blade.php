@extends('layouts.welcome_layout')

@section('styles')
<style type="text/css" media="screen">
    body{
        background: #222 !important; 
    }
    .background_image{
    background-size: 100%;
    background-repeat: no-repeat;
    }
    .mapa{
        margin-top: 70px;
    }
    @media (max-width: 769px){
        .mapa{
            margin-top: 0px;
        }
    }
</style>
@endsection

@section('content')
<div class="background_image" style="background-image: url('{{ asset('img/banner_2.jpeg') }}'); min-height: 400px; padding: 10px;">
    <div class="row">
    	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    		<div class="box box-primary" style="margin-top: 70px; background-color: rgba(255,255,255,0.9); color: #222;">
    			<div class="panel-body" style='min-height:400px;'>
                    <h1>Medios de Contacto</h1> 
                    <hr>
                    <div class="table-responsive">
        				<table class="table table-hover">
        					<tbody>
        						<tr>
                                    <td>
                                        <a target="black" href="https://www.google.com.ar/maps/place/Galer%C3%ADa+G%C3%A9minix/@-34.914282,-57.9519788,20.44z/data=!4m5!3m4!1s0x0:0x9cc929911681b21b!8m2!3d-34.9143498!4d-57.9520004">
                                            <i class="fa fa-map-marker fa-2x"></i>
                                        </a>
                                    </td>
                                    <td> 
                                        <a target="black" href="https://www.google.com/maps/place/Calle+9+720,+La+Plata,+Buenos+Aires,+Argentina/@-34.9144753,-57.9569488,17z/data=!3m1!4b1!4m5!3m4!1s0x95a2e636b87095d9:0x9b285d564a52cd37!8m2!3d-34.9144753!4d-57.9547601">
                                            <b>Dirección oficina (La Plata):</b>
                                        </a>
                                    </td>
                                    <td>Calle 48 n°633 entre 7 y 8 Galería Geminix Piso 9 Oficina 905</td></tr>
        						<tr><td><a href="mailto:mail@sisarg.website.com?subject=Consulta"><i class="fa fa-envelope-o fa-2x"></i></a></td> <td><b>Correo electrónico:</b></td><td><a href="mailto:mail@sisarg.website?subject=Consulta">mail@sisarg.website</a></td></tr>
        						{{-- <tr><td><i class="fa fa-mobile fa-2x"></i></td><td><b>Teléfonos:</b></td><td> (0221) 543-8218 - (0221) 631-4545</td></tr>
                                <tr><td><i class="fa fa-phone fa-2x"></i></td><td><b>Teléfono fijo:</b></td><td>(0221) 425-6990 </td></tr> --}}
        					    <tr>
                                    <td colspan="2">
                                        <a target="black" href="https://www.google.com.ar/maps/place/Galer%C3%ADa+G%C3%A9minix/@-34.914282,-57.9519788,20.44z/data=!4m5!3m4!1s0x0:0x9cc929911681b21b!8m2!3d-34.9143498!4d-57.9520004" 
                                        title="¿Como llegar?"
                                        class="btn btn-success btn-lg" 
                                        >
                                            <img src="{{ asset('img/como_llegar.png') }}"  height="40">&nbsp;&nbsp;
                                            Indicaciones Google Maps
                                        </a>
                                    </td>
                                </tr>
                            </tbody>
        				</table>
                    </div>
    			</div>
    		</div>
    	</div>
    	<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 mapa" >
    		<div >
                <div id="mimapholder"  style='height: 400px;'><h3> Cargando Mapa...</h3></div>
    		</div>
    		
    	</div>
    </div>
</div>
@endsection

@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDEcgpsl3Il7QBZzJQ2FHKBIau4FRmZndc&callback=initMap"></script>
    <script>
        initMap();
        var marker = null;
        var ubicacion_actual = {lat: -34.914405, lng: -57.952016 };
        placeMarker(map, ubicacion_actual);
        
        $(document).ready(function()
        {
            //mapa
            google.maps.event.trigger(map, 'resize');
            map.setCenter(new google.maps.LatLng(-34.914405, -57.952016));
            fixed = 1;

        });  //END document ready


        function initMap() 
        {
            myOptions={
            center:new google.maps.LatLng(-34.914405,-57.952016),
            zoom:15,
            mapTypeId:google.maps.MapTypeId.ROADMAP,
            mapTypeControl:false,
            navigationControlOptions:{style:google.maps.NavigationControlStyle.SMALL}
            } 
            map = new google.maps.Map(document.getElementById("mimapholder"), myOptions);
        }


        function placeMarker(map, location) 
        {
            if(marker)
            {
                marker.setPosition(location);
            }
            else
            {
                marker = new google.maps.Marker({
                    position: location,
                    icon: '{{ asset('img/icomapa.ico') }}',
                    map: map
                });
            }
        }


    </script>
@endsection
