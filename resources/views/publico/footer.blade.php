<div class="footer" >
	<div class="row">
		<div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
			<div>
				<h4>
					<img src="{{ asset('img/logo-sisarg-transparente-blanco.png') }}" width="200" alt="logo"> 
					<br><br>
					<small> &copy; 2015 Todos los derechos reservados</small>
					<br>
					<small>Pensamos y creamos en Argentina desde 2009</small> 
					<br>
					Solicite presupuesto sin cargos via correo electrónico o a travéz de la web.
					
				</h4>
			</div>
		</div>
		<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4" >
			<div>
				{{-- <h4>
					<img src="{{ asset('img/whatsapp.png') }}" width="35">
				 	&nbsp;	<span> +54 (221) 15-428-3302</span>
				</h4>--}}
				<a href="mailto:mail@sisarg.website?subject=Consulta" id="mailto">
					<p>Medios de contacto</p>
					<hr>
					<h4 style="color: #FFF;">
						<img src="{{ asset('img/email.png') }}" width="35">
					 	&nbsp;	mail@sisarg.website
					</h4>
				</a>
				<h4>
					<img src="{{ asset('img/icono-web.png') }}" width="35">
				 	&nbsp;	<span> sisarg.website</span>
				</h4>
			</div>
		</div>
	</div>
</div>