@extends('layouts.welcome_layout')

@section('styles')
<style type="text/css" media="screen">
    body{
        background: #222 !important; 
    }
    .background_image{
    background-size: 100%;
    background-repeat: no-repeat;
    }
</style>
@endsection

@section('content')
<div class="background_image" style="background-image: url({{ asset('img/banner_2.jpeg') }}); min-height: 400px; padding: 10px;">
    <div class="row">
    	<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
    		<div class="box box-primary" style="margin-top: 70px; background-color: rgba(255,255,255,0.9); color: #222;">
    			<div class="panel-body">
    				{!! Form::open(['route' => 'consultas_web.store']) !!}
    					<h4><b>Dejanos tu consulta o inquietud y te contestaremos a donde nos indiques lo antes posible.</b></h4>
    					
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        @if(session('status') != '')
                            <div class="alert {{session('class')}}" role="alert">
                              <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              {{session('status')}}
                            </div>
                        @endif
                        
    					<div class="form-group">
    						<label>Medio de contacto</label>
                            {!! Form::text('medio_comunicacion', null, ['class' => 'form-control', 'rows'=>4, 'placeholder'=>'E-mail, Teléfono, Whatsaap, etc...']) !!}
    					</div>
    					<div class="form-group">
    						<label>Tu nombre</label>
    						{!! Form::text('nombre', null, ['class' => 'form-control', 'rows'=>4, 'placeholder'=>'Tu nombre y apellido']) !!}
    					</div>
    					<div class="form-group">
    						<label>Tu consulta o sugerencia</label>
                            {!! Form::textarea('texto', null, ['class' => 'form-control', 'rows'=>4]) !!}
    					</div>
    					<div class="form-group">
    						<button type="submit" class="btn btn-primary"> Enviar consulta o inquietud</button>
    					</div>
    				{!! Form::close() !!}
    			</div>
    		</div>
    	</div>
    </div>
</div>
@endsection
