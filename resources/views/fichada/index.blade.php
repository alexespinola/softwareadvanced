@extends('layouts.welcome_layout')

@section('content')
	
<br><br>
<h3 class="text-center">Fichadas</h3>

<div class="table-responsive">
	<table class="table table-hover table-condensed">
		<thead>
			<tr class="info">
				<th>Semana/Día</th>
				<th>Entrada</th>
				<th>Salida</th>
				<th>Total</th>
			</tr>
		</thead>
		<tbody>
			@forelse ($fichada as $f)
				<tr>
					<td>{{$f['semana']}} / {{$f['dia']}}</td>
					<td>{{$f['entrada']->format('H:i:s')}}</td>
					@if (isset($f['salida']))
						<td>{{$f['salida']->format('H:i:s')}}</td>
					@else
						<td>-- : -- : --</td>
					@endif
					@if (isset($f['minutos']))
						<td>{{ gmdate('H:i:s', floor(( ($f['minutos']/60) * 3600)))   }}</td>
					@else
						<td>-- : -- : --</td>
					@endif
				</tr>
				
			@empty
				<tr>
					<td>NO hay registros</td>
				</tr>
			@endforelse
				
				<tr >
					<th class="success">TOTAL HORAS</th>
					<th class="success">{{$total}}</th>
					<th class="danger">FALTANTE</th>
					<th class="danger">{{$faltan}}</th>
				</tr>
		</tbody>
	</table>
</div>

@endsection
