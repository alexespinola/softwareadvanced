@extends('layouts.app')
@section('header_title', 'Detalle del cliente')
@section('header_subtitle', 'Muestra la información ampliada del cliente')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/clientes')}}"><i class="fa fa-users"></i> Clientes</a></li>
    <li class="active"> <i class="fa fa-eye"></i> Ver cliente</li>
  </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-body">
                    {!! Form::model($cliente, ['route' => ['clientes.index'], 'method' => 'get']) !!}

                        @include('clientes.show_fields')

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
