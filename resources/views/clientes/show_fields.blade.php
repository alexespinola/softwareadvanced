<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

    <div class="form-group">
        {!! Form::label('nombre', 'Nombre:') !!}
        {!! Form::text('nombre', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('apellido', 'Apellio:') !!}
        {!! Form::text('apellido', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('dni', 'DNI:') !!}
        {!! Form::text('dni', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('legajo', 'Legajo:') !!}
        {!! Form::text('legajo', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('fecha_nacimiento', 'Fecha de nacimiento:') !!}
          {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control datepicker', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('trabaja_en', 'Trabaja en:') !!}
          {!! Form::text('trabaja_en', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('direccion', 'Dirección:') !!}
          {!! Form::text('direccion', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('direccion_laboral', 'Dirección laboral:') !!}
          {!! Form::text('direccion_laboral', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_fijo', 'Teléfono fijo:') !!}
          {!! Form::text('tel_fijo', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_celular', 'Teléfono celular:') !!}
          {!! Form::text('tel_celular', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

</div>
<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

    <div class="form-group">
        {!! Form::label('tel_laboral', 'Teléfono laboral:') !!}
          {!! Form::text('tel_laboral', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_ref1', 'Teléfono Referido 1:') !!}
          {!! Form::text('tel_ref1', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_ref2', 'Teléfono Referido 2:') !!}
          {!! Form::text('tel_ref2', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_ref3', 'Teléfono Referido 3:') !!}
          {!! Form::text('tel_ref3', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('tel_ref4', 'Teléfono Referido 4:') !!}
          {!! Form::text('tel_ref4', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('email', 'E-mail:') !!}
          {!! Form::text('email', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('comentarios', 'Comentarios:') !!}
          {!! Form::textarea('comentarios', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <div class="form-group">
        {!! Form::label('created_at', 'Creado el:') !!}
          {!! Form::text('created_at', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>

    <!-- Submit Field -->
    <div class="form-group">
        <a href="{!! route('clientes.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
    </div>

</div>
