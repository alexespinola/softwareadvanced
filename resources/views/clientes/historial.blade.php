@extends('layouts.app')
@section('header_title', 'Histórico del cliente')
@section('header_subtitle', 'Muestra el listado de créditos del cliente')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/clientes')}}"><i class="fa fa-users"></i> Clientes</a></li>
    <li class="active"> <i class="fa fa-eye"></i> Ver historico</li>
  </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$cliente->nombre}} {{$cliente->apellido}}</h3>
                    <span>( DNI: {{$cliente->dni}} )</span>
                    <div class="box-tools pull-right">
                        <a href="{{ route('clientes.edit', $cliente->id) }}" class='btn btn-warning btn-sm'>
                            <i class="fa fa-pencil"></i>
                            Editar cliente
                        </a>
                        <a href="{{url('creditos/create')}}"  class="btn btn-primary btn-sm"  title=""><i class="fa fa-plus"></i> Agregar Crédito</a>
                      <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                      {{-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> --}}
                    </div>
                </div>
                <div class="box-body">
                    <div class="table-responsive">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>EMPRESA</th>
                                    <th>MONTO</th>
                                    <th>CUOTAS</th>
                                    <th>ESTADO</th>
                                    <th>FECHA DE EMISIÓN</th>
                                    <th>FECHA ÚLTIMA CUOTA</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($creditos as $e)
                                    <tr>
                                        <td>{{$e->empresa->nombre}}</td>
                                        <td>$ {{$e->monto}}</td>
                                        <td>{{$e->cuotas}}</td>
                                        <td>{{$e->estado->nombre}}</td>
                                        <td>{{date('d/m/Y',strtotime($e->fecha_emision))}}</td>
                                        <td>{{date('d/m/Y',strtotime($e->fecha_ultima_cuota))}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
