@extends('layouts.app')
@section('header_title', 'Alta de Clientes')
@section('header_subtitle', 'Inserta un registro en el Listado de clientes.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/clientes')}}"><i class="fa fa-users"></i> Clientes</a></li>
    <li class="active"> <i class="fa fa-plus"></i> Alta de clientes</li>
  </ol>
@endsection

@section('content')
    
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">
                
                {!! Form::open(['route' => 'clientes.store']) !!}

                    @include('clientes.fields')

                {!! Form::close() !!}
                
            </div>
        </div>
      </div>
    </div>
@endsection
