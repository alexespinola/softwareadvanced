@extends('layouts.app')
@section('header_title', 'Detalle Usuario')
@section('header_subtitle', 'Muestra detalle de un usuario.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/usuarios')}}"><i class="fa fa-users"></i> Usuasrios</a></li>
    <li class="active"> <i class="fa fa-eye"></i> Ver usuarios</li>
  </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="row" style="padding-left: 20px">
                        @include('users.show_fields')
                        <a href="{!! route('usuarios.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
