@extends('layouts.app')
@section('header_title', 'Modificación de Usuario')
@section('header_subtitle', 'Modifica un usuario del sistema.')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/usuarios')}}"><i class="fa fa-users"></i> Usuarios</a></li>
    <li class="active"> <i class="fa fa-pencil"></i> Modificación de usuario</li>
  </ol>
@endsection

@section('content')
    
    <div class="row">
      <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="box box-primary">

            <div class="box-body">

        	 	{!! Form::model($user, ['route' => ['usuarios.update', $user->id], 'method' => 'patch']) !!}

                    @include('users.fields')

               	{!! Form::close() !!}

            </div>
        </div>
      </div>
    </div>
@endsection
