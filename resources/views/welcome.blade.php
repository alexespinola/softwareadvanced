@extends('layouts.welcome_layout')
@section('styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('css/animate.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/fuentes.css') }}">
	
@endsection
@section('content')

<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="8000" >
  <!-- Wrapper for slides -->
  <div class="carousel-inner img_grande" role="listbox">
    <div class="item benner active">
      <h1 class="carousel_title">Tecnología, diseño e innovación</h1>
      <div class="carousel-caption">
          	<h3 data-animation="animated bounceInLeft" class="ctp">
            	SIS-ARG desarrolla sistemas informaticos y estrategias de comunicación digital.
            </h3>
            <h3 data-animation="animated bounceInLeft" class="ctp">
              Innovación, tecnología, arte y diseño son los pilares de nuestra empresa y filosofía.
            </h3>
          </div>
    </div>
    <div class="item benner">
      <h1 class="carousel_title">Nuestros servicios</h1>
      <div class="carousel-caption">
          	<h3 data-animation="animated bounceInLeft" class="ctp">
            	Desarrollo web y mobile, Integración de sistemas, Asistencia tecnológica
            </h3>
            <h3 data-animation="animated bounceInLeft" class="ctp">
              Diseño y Comunicación Visual, Marketing Digital, Bussines Intelligence 
            </h3>
            <h3 data-animation="animated bounceInLeft" class="ctp">
              E-Commerce, Big Data, Consultoría y Auditoría
            </h3>
          </div>
    </div>
  </div>

  <!-- Left and right controls -->
	 {{--  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left"></span>
	    <span class="sr-only">Previous</span>
	  </a> --}}
	  <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right"></span>
	    <span class="sr-only">Next</span>
	  </a>
</div>

<div class="row">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" >
		<div class="blok" style="padding-top: 0px; margin-top: 0px;">
			<h2 style="color: #026880;"><b>NUESTRA VISIÓN</b></h2>
			<p>El norte de nuestro equipo es acercar la tecnología a la sociedad al menor costo posible. 
				<br>Entendemos que la tecnología es un instrumento generador de transformaciones culturales y economicas y nos complace ser 
				<br>parte de esa evolución, por lo que tambien estamos muy atentos a los impactos negativos que nuestro trabajo podría generar. 
			</p>
		</div>
		
		<div class="blok gradiente2" style="/*background: #008d94;*/ color:#FFF;">
			<h2>METODOLOGÍA DE TRABAJO</h2>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="text-align: justify;">
					<p>
						Nuestro proceso de desarrollo está guiado por el 	<B>Manifiesto de Desarrollo Ágil de Software</B>, nuestra prioridad es la entrega temprana y continua de software con valor para cliente.
					</p>
					<ul>
						<li>
							Aceptamos que los requisitos cambien, incluso en etapas tardías del desarrollo. Los equipos Ágiles aprovechan el cambio para proporcionar ventaja competitiva al cliente.
						</li>
						<li>
							Nos esforzamos para entregar software funcional cada dos o tres semanas con el objetivo de someterlo a las consideraciones del cliente lo antes posible.
						</li>
						<li>
							Los responsables de negocio y los desarrolladores trabajamos juntos de forma cotidiana.
						</li>
						<li>
							Consideramos que el software funcionando que aporta valor al cliente es la medida principal de progreso del proyecto.
						</li>
						<li>
							La atención continua a la excelencia técnica y al buen diseño del software son claves para el éxito.
						</li>
						<li>
							A intervalos regulares el equipo reflexiona sobre cómo ser más efectivo para luego ajustar y perfeccionar su comportamiento en consecuencia.
						</li>
					</ul>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
					<img src="{{ asset('img/metodologia.png') }}" style="width: 100%" alt="Metodología de trabajo">
				</div>
			</div>
		</div>

		<div class="blok">
			<h2 style="color: #026880;"><b>TECNOLOGÍAS</b></h2>
			<p>Invertimos gran parte de nuestro tiempo en investigar y crear nuevas tecnologías,
			<br>pero la experiencia nos dice que usar estadares a nivel mundial tiene el beneficio de la cooperación de una gran comunidad de expertos,
			<br>por eso preferimos usar algunas de estas tecnologias siempre que se adapten a las necesidades de nuestros clientes.</p>
	
			<div class="row">
				<div class="col-lg-2 col-md-3 col-xs-6">
			      <!-- small box -->
			      <div class="small-box" style="background-color: #001F3F; color: #FFF;">
			        <div class="inner">
			          <h2>MySQL</h2>
			          <p>Relational data base</p>
			        </div>
			        <div class="icon">
			          <i class="fa fa-database"></i>
			        </div>
			      </div>
		    </div>
				<div class="col-lg-2 col-md-3 col-xs-6">
			      <!-- small box -->
			      <div class="small-box" style="background-color: #3c8dbc; color: #FFF;">
			        <div class="inner">
			          <h2>Mongo DB</h2>
			          <p>NO SQL data base</p>
			        </div>
			        <div class="icon">
			          <i class="fa fa-database"></i>
			        </div>
			      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <!-- small box -->
		      <div class="small-box" style="background-color: #00c0ef; color: #FFF;">
		        <div class="inner">
		          <h2>Postgres</h2>
		          <p>Relational data base</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-database"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#00a65a; color: #FFF;">
		        <div class="inner">
		          <h2>PHP</h2>
		          <p> WEB Back end tecnology</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#39CCCC; color: #FFF;">
		        <div class="inner">
		          <h2>Laravel</h2>
		          <p> PHP framework</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#00a600; color: #FFF;">
		        <div class="inner">
		          <h2 style="font-size: 25px;">Wordpress</h2>
		          <p> PHP CMS</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#00a600; color: #FFF;">
		        <div class="inner">
		          <h2>HTML 5</h2>
		          <p>lenguaje markup</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#63b963; color: #FFF;">
		        <div class="inner">
		          <h2>CSS 3</h2>
		          <p>Style markup</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#f39c12; color: #FFF;">
		        <div class="inner">
		          <h2>JQuery</h2>
		          <p>Java Script framework</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#f56954; color: #FFF;">
		        <div class="inner">
		          <h2>Angular 4</h2>
		          <p>Java Script framework</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#605ca8; color: #FFF;">
		        <div class="inner">
		          <h2>Vue JS</h2>
		          <p>Java Script framework</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#D81B60; color: #FFF;">
		        <div class="inner">
		          <h2>Node JS</h2>
		          <p>Java Script framework</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color:#8a6070; color: #FFF;">
		        <div class="inner">
		          <h2>Ionic 3</h2>
		          <p>Mobile framework</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color: #b38cb2; color: #FFF;">
		        <div class="inner">
		          <h2>Android Studio</h2>
		          {{-- <p>Mobile Development</p> --}}
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color: #D81B10; color: #FFF;">
		        <div class="inner">
		          <h2>git</h2>
		          <p> Version control system</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color: #9b55b7; color: #FFF;">
		        <div class="inner">
		          <h2>Google Analytics</h2>
		          <p>Analítica web</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color: #f39c12; color: #FFF;">
		        <div class="inner">
		          <h2>Google Maps</h2>
		          <p>Mapas interactivos</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-2 col-md-3 col-xs-6">
		      <div class="small-box" style="background-color: #63b963; color: #FFF;">
		        <div class="inner">
		          <h2>Mercado Pagos</h2>
		          <p> API de pagos online</p>
		        </div>
		        <div class="icon">
		          <i class="fa fa-code"></i>
		        </div>
		      </div>
		    </div>
			</div>
		</div>

		<div class="blok gradiente" > 
			<h2 style="color: #026880;"><b>SERVICIOS</b></h2>
			{{-- <p>La tecnología al servicio del hombre y la empresa</p> --}}
		    
		  <div class="row" style="padding-top: 10px; padding-bottom: 10px;">
				<div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-green"><i class="ion ion-ios-cart-outline"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">E-COMMERCE</span>
		          <span class="">Todas las formas de pago en un solo clic</span>
		        </div>
		      </div>
		    </div>
				<div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-blue"><i class="fa fa-globe"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">GEO LOCALIZACIÓN</span>
		          <span class="">Mapas interactívos <br> Seguimiento de vehículos</span>
		        </div>
		      </div>
		    </div>
	    	<div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-red"><i class="fa fa-certificate"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">BRANDING</span>
		          <span class="">Posicionamiento de marca <br> Presencia en redes sociales </span>
		        </div>
		      </div>
		    </div>
				<div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-orange"><i class="fa fa-building"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">BUSSINES INTELLIGENCE</span>
		          <span class="">Arquitectura para la empresa</span>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-orange"><i class="fa fa-database"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">BIG DATA</span>
		          <span class="">Analisis de la información </span>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-orange"><i class="fa fa-mobile"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">DESARROLLO WEB MOBILE</span>
		          <span class=""> Responsive Design <br> APP's multiplataformas</span>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-qrcode"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">SISTEMAS DE STOCK</span>
		          <span class="">Almacenamiento centralizado <br> Pistolas lectoras de códigos </span>
		        </div>
		      </div>
		    </div>
		    <div class="col-lg-3 col-md-6  col-sm-12 col-xs-12">
		      <div class="info-box">
		        <span class="info-box-icon bg-aqua"><i class="glyphicon glyphicon-wrench"></i></span>
		        <div class="info-box-content">
		          <span class="info-box-number">CONSULTORÍA y AUDITORÍA</span>
		          <span class="">Asistencia tecnológica <br> Controld de procesos de calidad </span>
		        </div>
		      </div>
		    </div>
			</div>
		</div>

		<div class="blok">
			<h2>ALGUNOS DE NUESTROS TRABAJOS</h2>

			<div id="carousel-clientes" class="carousel slide" data-ride="carousel" data-interval="4000" >
			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			    <div class="item active">
			    	<div class="row">
				    	<div class="col-xs-12 col-sm-2 col-md-1 col-lg-1">
				    		<img src="{{ asset('img/clientes/gba.svg') }}" height="90" alt="gobierno de buenes aires">
				    	</div>
				    	<div class="col-xs-12 col-sm-9 col-md-11 col-lg-11">
				    		<h3 style="padding: 10px;">Registro unificado de bienes muebles e inmuebles de la provincia de buenos aires.</h3>
				    	</div>
			    	</div>
			    </div>

			    <div class="item">
			    	<div class="row">
				      <div class="col-xs-12 col-sm-2 col-md-1 col-lg-1">
				    		<img src="{{ asset('img/clientes/ministerio_transporte.png') }}"  height="90"  alt="gobierno de buenes aires">
				    	</div>
				    	<div class="col-xs-12 col-sm-9 col-md-11 col-lg-11">
				    		<h3 style="padding: 10px;">Sistema de ventas web de pasajes de trenes de larga distancia.</h3>
				    	</div>
			    	</div>
			    </div>
			    <div class="item">
			    	<div class="row">
				      <div class="col-xs-12 col-sm-2 col-md-1 col-lg-1">
				    		<img src="{{ asset('img/clientes/operadora-ferroviaria.png') }}"  height="50"  alt="gobierno de buenes aires">
				    	</div>
				    	<div class="col-xs-12 col-sm-9 col-md-11 col-lg-11">
				    		<h3 style="padding: 10px;">Sistema unifiacado de registro de proveedores, Intranet corporativa </h3>
				    	</div>
			    	</div>
			    </div>

			    <div class="item">
			    	<div class="row">
				      <div class="col-xs-12 col-sm-2 col-md-1 col-lg-1">
				    		<img src="{{ asset('img/clientes/c21-es.png') }}"  height="60"  alt="gobierno de buenes aires">
				    	</div>
				    	<div class="col-xs-12 col-sm-9 col-md-11 col-lg-11">
				    		<h3 style="padding: 10px;">Web site inmobiliario con sistema de autogestión</h3>
				    	</div>
			    	</div>
			    </div>
			    <div class="item">
			    	<div class="row">
				      <div class="col-xs-12 col-sm-2 col-md-1 col-lg-1">
				    		<img src="{{ asset('img/clientes/colegio_ingenieros.png') }}"  height="60"  alt="gobierno de buenes aires">
				    	</div>
				    	<div class="col-xs-12 col-sm-9 col-md-11 col-lg-11">
				    		<h3 style="padding: 10px;">Portal web del colegio de ingenieros de la provincia de Buenos Aires.</h3>
				    	</div>
			    	</div>
			    </div>
			  </div>

			  <!-- Left and right controls -->
				 {{--  <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
				    <span class="glyphicon glyphicon-chevron-left"></span>
				    <span class="sr-only">Previous</span>
				  </a> --}}
				  <a class="right carousel-control" href="#carousel-clientes" data-slide="next">
				    <span class="glyphicon glyphicon-chevron-right"></span>
				    <span class="sr-only">Next</span>
				  </a>
			</div>
		</div>
	</div>
</div>

@endsection

@section('scripts')
	<script src="{{ asset('js/animate.js') }}" type="text/javascript" charset="utf-8" async defer></script>
@endsection

