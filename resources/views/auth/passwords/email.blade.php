@extends('layouts.welcome_layout')

@section('styles')
<style type="text/css" media="screen">
  
    body{
        background: #222 !important; 
    }
    .background_image{
        background-size: 100%;
        background-repeat: no-repeat;
        min-height: 450px;
    }
   
</style>
@endsection

@section('content')
<div class="background_image" style="background-image: url('{{ asset('img/banner_2.jpeg') }}');  padding: 10px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="box box-primary" style="margin-top: 100px;">
                    <div class="box-header with-border" style="background-color: #333; color: #DDD;">
                        <h3 class="box-title">Recuperar contraseña</h3>
                    </div>
                    <div class="box-body" style="background-color: #333; color: #DDD;">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail </label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary btn-block">
                                        Enviar E-Mail para recuperar la contraseña
                                    </button>
                                    <br>
                                    <a href="{{url('/login')}}" class="btn btn-danger btn-block">Cancelar</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
