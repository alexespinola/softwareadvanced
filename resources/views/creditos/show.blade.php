@extends('layouts.app')
@section('header_title', 'Detalle Credito')
@section('header_subtitle', 'Muestra detalle de un crédito')

@section('camino')
  <ol class="breadcrumb">
    <li><a href="{{url('/home')}}"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="{{url('/empresas')}}"><i class="fa fa-money"></i> Créditos</a></li>
    <li class="active"> <i class="fa fa-eye"></i> Ver crédito</li>
  </ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
            <div class="box box-primary">
                <div class="box-body">
                    {!! Form::model($credito, ['route' => ['empresas.index'], 'method' => 'patch']) !!}
                        @include('creditos.show_fields')
                        <a href="{!! route('creditos.index') !!}" class="btn btn-default"><i class="fa fa-undo"></i> Regresar</a>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
