<div class="form-group">
    {!! Form::label('cliente_id', 'Cliente:') !!}
    {!! Form::select('cliente_id', $clientes, null, ['class'=>'form-control select2', 'disabled'=>'disabled']) !!}
</div>


<div class="form-group">
    {!! Form::label('empresa_id', 'Empresa:') !!}
    {!! Form::select('empresa_id', $empresas, null, ['class'=>'form-control select2', 'disabled'=>'disabled']) !!}
</div>


<div class="form-group">
    {!! Form::label('fecha_emision', 'Fecha de emispión:') !!}
    {!! Form::text('fecha_emision', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Fecha de Emisión', 'disabled'=>'disabled']) !!}
</div>

<div class="form-group">
    {!! Form::label('monto', 'Monto:') !!}
    {!! Form::text('monto', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
</div>

<div class="form-group">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 0px;">
        {!! Form::label('cuotas', 'Cuotas:') !!}
        {!! Form::number('cuotas', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0px; margin-bottom: 15px;">
        {!! Form::label('cuota_monto', 'Monto de la cuota:') !!}
        <div class="input-group">
            <span class="input-group-addon">$</span>
            {!! Form::text('cuota_monto', null, ['class' => 'form-control', 'disabled'=>'disabled']) !!}
        </div>
        
    </div>
</div>


<div class="form-group">
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-left: 0px;">
        {!! Form::label('fecha_ultima_cuota', 'Fecha de la última cuota:') !!}
        {!! Form::text('fecha_ultima_cuota', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Fecha de la última cuota', 'disabled'=>'disabled']) !!}
    </div>
    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6" style="padding-right: 0px; margin-bottom: 15px;">
        {!! Form::label('fecha_renovacion', 'Fecha en la que puede renovar:') !!}
        {!! Form::text('fecha_renovacion', null , ['class'=>'form-control datepicker' , 'placeholder'=>'Fecha en la que puede renovar', 'disabled'=>'disabled']) !!}
    </div>
</div>



<div class="form-group">
    {!! Form::label('estado', 'Estado:') !!}
    {!! Form::select('estado', $estados, null, ['class'=>'form-control select2', 'disabled'=>'disabled']) !!}
</div>


<div class="form-group">
    {!! Form::label('user_id', 'Gestionado Por:') !!}
    {!! Form::select('user_id', $usuarios, null, ['class'=>'form-control select2', 'disabled'=>'disabled']) !!}
</div>