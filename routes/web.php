<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout', 'Auth\LoginController@logout');
Route::resource('/consultas_web', 'ConsultasController');

Route::get('/contacto', function () { return view('publico.contacto'); });
Route::get('/consultas', function () { return view('publico.consultas'); });

Route::get('/', function () {
    return view('welcome');
    // return redirect('/login');
});



Route::group(['middleware' => 'auth'], function()
{
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/panel', 'PanelController@index')->name('panel');
	Route::get('/clientes/search', 'ClientesController@search');
	Route::get('/clientes/historial/{cliente_id}', 'ClientesController@historial');
	Route::resource('/clientes', 'ClientesController');
	Route::resource('/empresas', 'EmpresasController');
	Route::resource('/creditos', 'CreditosController');
	Route::resource('/usuarios', 'UsersController');
	Route::get('/get_pdf/{id}', 'CreditosController@pdf');
});


Route::resource('/fichada', 'FichadaController');
