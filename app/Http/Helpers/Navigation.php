<?php


    function isActiveRoute($route, $output = 'active')
    {
        if (Route::getCurrentRoute()->uri() == $route) {
            return $output;
        }
    }

    function areActiveRoutes(Array $routes, $output = 'active')
	{
	    foreach ($routes as $route)
	    {
	        if (Route::getCurrentRoute()->getPath() == $route) return $output;
	    }

	}
