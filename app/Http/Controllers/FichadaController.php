<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fichada;
use Carbon\Carbon;

class FichadaController extends Controller
{
   
  /**
   * mostrar
   */
	public function index(Request $req)
  {
  	$fichada = array();
  	if (isset($req->semana)) 
  	{
  		$fichada = Fichada::orderBy('created_at')->where('semana', $req->semana)->get();
  	}
  	else
  	{
  		$fichada = Fichada::orderBy('created_at')->where('semana', date('W'))->get();
  	}

  	foreach ($fichada as $f) {
  		$dia = Carbon::createFromFormat('Y-m-d H:i:s', $f->created_at);
  		$row[$dia->format('w')][$f->tipo] = $dia;
  		$row[$dia->format('w')]['dia'] = $dia->format('D-d');
  		$row[$dia->format('w')]['semana'] = $dia->format('W');
  		$row[$dia->format('w')]['tipo'] = $f->tipo;
  	}

  	$suma_min = 0;
    if (!isset($row)) {
      $data['total'] = '00:00:00';
      $data['faltan'] = '40:00:00';
      $data['fichada'] = array();
      return  view('fichada.index')->with($data);
    }

  	foreach ($row as $k => $r) {
      if (isset($r['entrada']) && isset($r['salida']) ) {
        $row[$k]['minutos'] = $r['entrada']->diffInMinutes($r['salida']);
        $suma_min += $row[$k]['minutos'];
      }
  	}

  	$data['total'] = intdiv($suma_min, 60).':'.gmdate('i:s', floor(($suma_min/60) * 3600));
  	$data['faltan']= intval(40-($suma_min/60)).':'.gmdate('i:s', floor((40 - ($suma_min/60)) * 3600));
  	$data['fichada'] = $row;
 		return  view('fichada.index')->with($data);
  }


  /**
   * fichar
   */
  public function create(Request $req)
  {
  	$fichada_anterior =  Fichada::where('tipo', $req->tipo)->orderBy('created_at', 'desc')->first();
  	if ($fichada_anterior) 
  	{
  		$hora_fichada_anterior = Carbon::createFromFormat('Y-m-d H:i:s', $fichada_anterior->created_at);
  		$now = Carbon::now();
  		if( $hora_fichada_anterior->diffInHours($now) < 1){
  			return '<br><h1 style="color: green; font-size: 100px; text-align-last: center;">Ya Fichaste</h1>';
  		}   

  	}


   	$f = new Fichada;
 		$f->tipo = $req->tipo;
 		$f->semana = date('W');
 		$f->save();
 		if (isset($f->id)) {
 			return '<br><h1 style="color: blue; font-size: 100px; text-align-last: center;">'. $req->tipo .' registrada</h1>';
 		}else{
 			return '<br><h1 style="color: red; font-size: 100px; text-align-last: center;">ERROR!</h1>';
 		}
  }

}
