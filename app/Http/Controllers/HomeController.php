<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Credito;
use Carbon\Carbon;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*fechas*/
        $primero_de_mes = Carbon::now()->startOfMonth();
        $fin_de_mes = Carbon::now()->endOfMonth();
        $primero_de_mes_anterior =  Carbon::now()->startOfMonth()->subMonths(1);
        $fin_de_mes_anterior =  Carbon::now()->startOfMonth()->subMonths(1)->endOfMonth();
        $now = Carbon::now();
        $subYears = Carbon::now()->subYears(1);
        $dps = array();
        $meses = array();

        /*total creditosd del mes*/
        $data['creditos_del_mes'] = Credito::where('fecha_emision','>=', $primero_de_mes)->where('fecha_emision','<=', $fin_de_mes)->count();
        $data['creditos_cobrados'] = Credito::where('fecha_emision','>=', $primero_de_mes)->where('fecha_emision','<=', $fin_de_mes)->where('estado_id',4)->count();
        $data['creditos_pendientes_de_cobro'] = Credito::where('fecha_emision','>=', $primero_de_mes)->where('fecha_emision','<=', $fin_de_mes)->where('estado_id',3)->count();
        $data['creditos_cancelados'] = Credito::where('fecha_emision','>=', $primero_de_mes)->where('fecha_emision','<=', $fin_de_mes)->where('estado_id',6)->count();
        $data['creditos_liquidados'] = Credito::where('fecha_emision','>=', $primero_de_mes_anterior)->where('fecha_emision','<=', $fin_de_mes_anterior)->where('estado_id',5)->count();

        /*DATA para el grafico*/
        $sql="SELECT count(c.id) cantidad , e.nombre empresa, YEAR(c.fecha_emision) y,  MONTH(c.fecha_emision) m
                FROM creditos as c
                LEFT JOIN empresas  as e ON e.id = c.empresa_id
                WHERE c.fecha_emision >= '".$subYears."'
                AND c.fecha_emision <= '".$now."'
                GROUP BY  YEAR(c.fecha_emision),  MONTH(c.fecha_emision), e.nombre
                ORDER BY  YEAR(c.fecha_emision),  MONTH(c.fecha_emision), e.nombre";

        $result = DB::select($sql);
        foreach ($result as $r) 
        {
            $dps[$r->empresa][$r->m.'-'.$r->y] = $r->cantidad;
            $meses[$r->m.'-'.$r->y] = $r->m.'-'.$r->y;
        }

        $data['dps'] = $dps;
        $data['meses'] = $meses;
        
        return view('home')->with($data);
    }
}
