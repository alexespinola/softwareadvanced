<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Credito;
use App\Empresa;
use App\Cliente;
use App\Estado;
use App\User;
use Carbon\Carbon;
use DateTime;
use Auth;
// use App\Libraries\FPDF\FPDF;
// use App\Libraries\FPDI\FPDI;
use Fpdf;

class CreditosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        // $filtros['user_id']         = array('text'=>$request->get('user_id'), 'tipo'=>'number');
        $filtros['cliente_id']      = array('text'=>$request->get('cliente_id'), 'tipo'=>'number');
        $filtros['empresa_id']      = array('text'=>$request->get('empresa_id'), 'tipo'=>'number');
        $filtros['monto']           = array('text'=>$request->get('monto'), 'tipo'=>'number');
        if ($request->get('fecha_emision')) 
        {
            $fecha_emision = DateTime::createFromFormat('d/m/Y', $request->get('fecha_emision'))->format('Y-m-d');
            $filtros['fecha_emision']   = array('text'=>$fecha_emision, 'tipo'=>'number');
        }
        $filtros['estado_id']          = array('text'=>$request->get('estado_id'), 'tipo'=>'text');


        $data['creditos'] = Credito::filtros($filtros)->orderBy('fecha_emision', 'DESC')->paginate(10);



        $clientes = Cliente::all();
        $data['clientes'] = array(0=>'Todos');
        foreach ($clientes as $key => $c) 
        {
            $data['clientes'][$c->id] = $c->nombre.' '.$c->apellido;
        }
       
        $empresas = Empresa::all();
        $data['empresas'] = array(0=>'Todas');
        foreach ($empresas as $key => $e) 
        {
            $data['empresas'][$e->id] = $e->nombre;
        }

        $estados = Estado::all();
        $data['estados'] = array(0=>'Todos');
        foreach ($estados as $key => $s) 
        {
            $data['estados'][$s->id] = $s->nombre;
        }

        return view('creditos.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clientes = Cliente::all();
        $data['clientes'] = array();
        foreach ($clientes as $key => $c) 
        {
            $data['clientes'][$c->id] = $c->nombre.' '.$c->apellido;
        }
       
        $empresas = Empresa::all();
        $data['empresas'] = array();
        foreach ($empresas as $key => $e) 
        {
            $data['empresas'][$e->id] = $e->nombre;
        }

        $estados = Estado::all();
        $data['estados'] = array();
        foreach ($estados as $key => $s) 
        {
            $data['estados'][$s->id] = $s->nombre;
        }

        $usuarios = User::all();
        $data['usuarios'] = array();
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }

        $data['accion'] = "alta";
        return view('creditos.create')->with($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'cliente_id'        => 'required|integer',
            'empresa_id'        => 'required|integer',
            'cuotas'            => 'required|integer',
            'monto'             => 'required|numeric',
            'cuota_monto'       => 'required|numeric',
            'fecha_emision'     => 'required',
            'fecha_ultima_cuota'=> 'required',
        ]);


        $credito = new Credito;
        $fecha_emision = DateTime::createFromFormat('d/m/Y', $request->fecha_emision);
        $fecha_ultima_cuota = DateTime::createFromFormat('d/m/Y', $request->fecha_ultima_cuota);
        $fecha_renovacion = DateTime::createFromFormat('d/m/Y', $request->fecha_renovacion);
        $credito->cliente_id = $request->cliente_id;
        $credito->empresa_id = $request->empresa_id;
        $credito->cuotas = $request->cuotas;
        $credito->cuota_monto = $request->cuota_monto;
        $credito->monto = $request->monto;
        $credito->fecha_emision = $fecha_emision;
        $credito->fecha_ultima_cuota = $fecha_ultima_cuota;
        $credito->fecha_renovacion = $fecha_renovacion;
        $credito->estado_id = $request->estado_id;
        $credito->user_id = Auth::user()->id;
        $credito->save();

        $request->session()->flash('status', 'Credito creado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('creditos.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clientes = Cliente::all();
        $data['clientes'] = array();
        foreach ($clientes as $key => $c) 
        {
            $data['clientes'][$c->id] = $c->nombre.' '.$c->apellido;
        }
       
        $empresas = Empresa::all();
        $data['empresas'] = array();
        foreach ($empresas as $key => $e) 
        {
            $data['empresas'][$e->id] = $e->nombre;
        }

        $estados = Estado::all();
        $data['estados'] = array();
        foreach ($estados as $key => $s) 
        {
            $data['estados'][$s->id] = $s->nombre;
        }

        $usuarios = User::all();
        $data['usuarios'] = array();
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }

        $credito = Credito::find($id);
        $credito->fecha_emision =  DateTime::createFromFormat('Y-m-d', $credito->fecha_emision)->format('d/m/Y');
        $credito->fecha_ultima_cuota =  DateTime::createFromFormat('Y-m-d', $credito->fecha_ultima_cuota)->format('d/m/Y');
        $data['credito'] = $credito;

        return view('creditos.show')->with($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clientes = Cliente::all();
        $data['clientes'] = array();
        foreach ($clientes as $key => $c) 
        {
            $data['clientes'][$c->id] = $c->nombre.' '.$c->apellido;
        }
       
        $empresas = Empresa::all();
        $data['empresas'] = array();
        foreach ($empresas as $key => $e) 
        {
            $data['empresas'][$e->id] = $e->nombre;
        }

        $estados = Estado::all();
        $data['estados'] = array();
        foreach ($estados as $key => $s) 
        {
            $data['estados'][$s->id] = $s->nombre;
        }

        $usuarios = User::all();
        $data['usuarios'] = array();
        foreach ($usuarios as $key => $u) 
        {
            $data['usuarios'][$u->id] = $u->name;
        }

        $credito = Credito::find($id);
        $credito->fecha_emision =  DateTime::createFromFormat('Y-m-d', $credito->fecha_emision)->format('d/m/Y');
        $credito->fecha_ultima_cuota =  DateTime::createFromFormat('Y-m-d', $credito->fecha_ultima_cuota)->format('d/m/Y');
        $credito->fecha_renovacion =  DateTime::createFromFormat('Y-m-d', $credito->fecha_renovacion)->format('d/m/Y');
        $data['credito'] = $credito;

        $data['accion'] = "edit";
        return view('creditos.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'cliente_id'        => 'required|integer',
            'empresa_id'        => 'required|integer',
            'cuotas'            => 'required|integer',
            'cuota_monto'       => 'required|numeric',
            'monto'             => 'required|numeric',
            'fecha_emision'     => 'required',
            'fecha_ultima_cuota'=> 'required',
        ]);


        $credito =  Credito::find($id);
        $fecha_emision = DateTime::createFromFormat('d/m/Y', $request->fecha_emision);
        $fecha_ultima_cuota = DateTime::createFromFormat('d/m/Y', $request->fecha_ultima_cuota);
        $fecha_renovacion = DateTime::createFromFormat('d/m/Y', $request->fecha_renovacion);
        $credito->cliente_id = $request->cliente_id;
        $credito->empresa_id = $request->empresa_id;
        $credito->cuotas = $request->cuotas;
        $credito->cuota_monto = $request->cuota_monto;
        $credito->monto = $request->monto;
        $credito->fecha_emision = $fecha_emision;
        $credito->fecha_ultima_cuota = $fecha_ultima_cuota;
        $credito->fecha_renovacion = $fecha_renovacion;
        $credito->estado_id = $request->estado_id;
        // $credito->user_id = Auth::user()->id;
        $credito->update();

        $request->session()->flash('status', 'Credito actualizado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('creditos.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $credito =  Credito::find($id);
        $credito->delete();

        $request->session()->flash('status', 'Credito Eliminado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('creditos.index'));
    }


    /**
     * PDF
     */
    public function pdf($id)
    {
        $credito = Credito::find($id);
        if (strtoupper($credito->empresa->nombre) == 'AMUPAP') 
        {
           $this->pdf_amupap($id);
        }
    }


    public function pdf_amupap($id)
    {
        $c = Credito::find($id);
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/01.png'),0,0,210);
        Fpdf::SetFont('Courier', 'B', 10);
        // Fpdf::SetFillColor(80 , 80, 80);
        Fpdf::SetXY(38, 136);
        Fpdf::Cell(45, 5, $c->cliente->apellido ,0,1,'L',0);
        Fpdf::SetXY(104, 136);
        Fpdf::Cell(40, 5, $c->cliente->nombre ,0,1,'L',0);
        Fpdf::SetXY(157, 136);
        Fpdf::Cell(40, 5, $c->cliente->dni ,0,1,'L',0);
        Fpdf::SetXY(39, 142);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->direccion) ,0,1,'L',0);
        Fpdf::SetXY(45, 153);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->tel_fijo) ,0,1,'L',0);
        Fpdf::SetXY(95, 153);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->tel_celular) ,0,1,'L',0);
        Fpdf::SetXY(164, 153);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->tel_laboral) ,0,1,'L',0);
        Fpdf::SetXY(42, 159);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->trabaja_en) ,0,1,'L',0);
        Fpdf::SetXY(122, 159);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->direccion_laboral) ,0,1,'L',0);
        Fpdf::SetXY(49, 169);
        Fpdf::Cell(40, 5, date('d/m/Y', strtotime($c->cliente->fecha_nacimiento)) ,0,1,'L',0);
        Fpdf::SetXY(106, 169);
        Fpdf::Cell(40, 5, $c->cliente->email ,0,1,'L',0);


        // pag2
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/02.png'),0,0,210);
        Fpdf::SetXY(89, 196);
        Fpdf::Cell(40, 5, 'La Plata' ,0,1,'L',0);
        Fpdf::SetXY(77, 233);
        Fpdf::Cell(40, 5,  utf8_decode($c->cliente->nombre .' '.$c->cliente->apellido) ,0,1,'L',0);
        Fpdf::SetXY(156, 233);
        Fpdf::Cell(40, 5, $c->cliente->dni  ,0,1,'L',0);

        //pag 3
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/03.png'),0,0,210);
        Fpdf::SetXY(58, 256);
        Fpdf::Cell(40, 5,  utf8_decode($c->cliente->nombre .' '.$c->cliente->apellido) ,0,1,'L',0);
        Fpdf::SetXY(106, 256);
        Fpdf::Cell(40, 5, $c->cliente->tel_fijo .' / '.$c->cliente->tel_celular ,0,1,'L',0);

        //pag 4
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/04.png'),0,0,210);
        Fpdf::SetXY(72, 148);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->apellido .' '.$c->cliente->nombre) ,0,1,'L',0);
        Fpdf::SetXY(72, 154);
        Fpdf::Cell(40, 5, $c->cliente->dni ,0,1,'L',0);
        Fpdf::SetXY(40, 192);
        Fpdf::Cell(40, 5, utf8_decode($c->cliente->direccion) ,0,1,'L',0);

        //pag 5
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/05.png'),0,0,210);

        //pag 6
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/06.png'),0,0,210);

        //pag 7
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/07.png'),0,0,210);

        //pag 8
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/08.png'),0,0,210);

        //pag 9
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/09.png'),0,0,210);

        //pag 10
        Fpdf::AddPage();
        Fpdf::Image(asset('pdfs/amupap/10.png'),0,0,210);

        

        Fpdf::Output();
        exit;
    }

}
