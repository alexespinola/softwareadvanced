<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Consulta;
use Illuminate\Support\Facades\Mail;
use App\Mail\Consultas as Mail_consulta;
use Exception;

class ConsultasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtros['nombre']           = array('text'=>$request->get('nombre'), 'tipo'=>'text');
        $filtros['medio_comunicacion']   = array('text'=>$request->get('medio_comunicacion'), 'tipo'=>'text');
        $filtros['texto']            = array('text'=>$request->get('texto'), 'tipo'=>'text');

        $data['consultas'] = Consulta::filtros($filtros)->orderBy('id', 'desc')->paginate(10);
        return view('consultas.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'medio_comunicacion' => 'required',
            'texto'=>'required'
        ]);

        $consulta = new Consulta;
        $consulta->medio_comunicacion = $request->medio_comunicacion;
        $consulta->nombre = $request->nombre;
        $consulta->texto = $request->texto;
        $consulta->save();

        try {
            Mail::to('alexaespinola@gmail.com')->send(new Mail_consulta($consulta));
        } catch (Exception $e) {
        }


        $request->session()->flash('status', 'Recibimos tu consulta. No tardaremos en contactarnos con vos.');
        $request->session()->flash('class', 'alert-success');

        return redirect(url('consultas'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $consulta = Consulta::find($id);
        $consulta->delete();

        $request->session()->flash('status', 'Consulta Eliminada correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('consultas_web.index'));
    }
}
