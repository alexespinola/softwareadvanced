<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use App\Cliente;
use DateTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Collective\Html\Eloquent\FormAccessible;


class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $filtros['nombre']      = array('text'=>$request->get('nombre'), 'tipo'=>'text');
        $filtros['apellido']    = array('text'=>$request->get('apellido'), 'tipo'=>'text');
        $filtros['dni']         = array('text'=>$request->get('dni'), 'tipo'=>'text');
        $filtros['tel_fijo']    = array('text'=>$request->get('tel_fijo'), 'tipo'=>'text');
        $filtros['tel_celular'] = array('text'=>$request->get('tel_celular'), 'tipo'=>'text');
        $filtros['email']       = array('text'=>$request->get('email'), 'tipo'=>'text');

        $data['clientes'] = Cliente::filtros($filtros)->paginate(10);
        return view('clientes.index')->with($data);
    }


    /**
     * Search clientes
     */
    public function search(Request $request)
    {
        $filtros['nombre']    = array('text'=>$request->get('search'), 'tipo'=>'text');
        $filtros['apellido']    = array('text'=>$request->get('search'), 'tipo'=>'text');
        $filtros['dni']         = array('text'=>$request->get('search'), 'tipo'=>'text');
        $filtros['tel_fijo']    = array('text'=>$request->get('search'), 'tipo'=>'text');
        $filtros['tel_celular'] = array('text'=>$request->get('search'), 'tipo'=>'text');
        $filtros['email']       = array('text'=>$request->get('search'), 'tipo'=>'text');

        $data['clientes'] = Cliente::where('nombre', $request->get('search'))->Search($filtros)->paginate(10);
        return view('clientes.index')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'apellido' => 'required',
            'dni' => 'required',
            'fecha_nacimiento' => 'required',
        ]);

        $cliente = new Cliente;
        $cliente->nombre = $request->nombre;
        $cliente->apellido = $request->apellido;
        $cliente->dni = $request->dni;
        $cliente->legajo = $request->legajo;
        $fecha_nacimiento = DateTime::createFromFormat('d/m/Y', $request->fecha_nacimiento);
        // echo $fecha->format('d-m-Y');
        $cliente->fecha_nacimiento = $fecha_nacimiento;
        $cliente->trabaja_en = $request->trabaja_en;
        $cliente->direccion = $request->direccion;
        $cliente->direccion_laboral = $request->direccion_laboral;
        $cliente->tel_fijo = $request->tel_fijo;
        $cliente->tel_celular = $request->tel_celular;
        $cliente->tel_laboral = $request->tel_laboral;
        $cliente->tel_ref1 = $request->tel_ref1;
        $cliente->tel_ref2 = $request->tel_ref2;
        $cliente->tel_ref3 = $request->tel_ref3;
        $cliente->tel_ref4 = $request->tel_ref4;
        $cliente->email = $request->email;
        $cliente->comentarios = $request->comentarios;
        $cliente->save();

        $request->session()->flash('status', 'Cliente creado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('clientes.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        $cliente->fecha_nacimiento =  DateTime::createFromFormat('Y-m-d', $cliente->fecha_nacimiento)->format('d/m/Y');
        // $cliente->created_at = Carbon::parse($cliente->created_at)->format('d/m/Y');
        // return  Carbon::parse($cliente->created_at)->format('d/m/Y');
        return view('clientes.show')->with('cliente', $cliente);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        $cliente->fecha_nacimiento =  DateTime::createFromFormat('Y-m-d', $cliente->fecha_nacimiento)->format('d/m/Y');
        return view('clientes.edit')->with('cliente', $cliente);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'nombre' => 'required|max:255',
            'apellido' => 'required',
            'dni' => 'required',
            'fecha_nacimiento' => 'required',
        ]);

        $cliente = Cliente::find($id);
        $cliente->nombre = $request->nombre;
        $cliente->apellido = $request->apellido;
        $cliente->dni = $request->dni;
        $cliente->legajo = $request->legajo;
        $fecha_nacimiento = DateTime::createFromFormat('d/m/Y', $request->fecha_nacimiento);
        // echo $fecha->format('d-m-Y');
        $cliente->fecha_nacimiento = $fecha_nacimiento;
        $cliente->trabaja_en = $request->trabaja_en;
        $cliente->direccion = $request->direccion;
        $cliente->direccion_laboral = $request->direccion_laboral;
        $cliente->tel_fijo = $request->tel_fijo;
        $cliente->tel_celular = $request->tel_celular;
        $cliente->tel_laboral = $request->tel_laboral;
        $cliente->tel_ref1 = $request->tel_ref1;
        $cliente->tel_ref2 = $request->tel_ref2;
        $cliente->tel_ref3 = $request->tel_ref3;
        $cliente->tel_ref4 = $request->tel_ref4;
        $cliente->email = $request->email;
        $cliente->comentarios = $request->comentarios;
        $cliente->update();

        $request->session()->flash('status', 'Cliente modificado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('clientes.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->delete();

        $request->session()->flash('status', 'Cliente Eliminado correctamente');
        $request->session()->flash('class', 'alert-success');

        return redirect(route('clientes.index'));
    }

    /**
     * muestra el historial del cliente
     */
    public function historial(Request $request, $id)
    {
        $data['cliente'] = Cliente::find($id);
        $data['creditos'] = $data['cliente']->creditos->reverse(); 
        return view('clientes.historial')->with($data);
    }
}
