<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Credito extends Model
{
    public function scopeFiltros($query, $filtros)
    {
        // dd($filtros);

        foreach ($filtros as $name => $filtro) 
        {
        	if ($filtro['text']) 
        	{
        	
	            if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'text' )
	            {
	            	$string = $filtro['text'];
	                $query->where($name, 'LIKE' , "%$string%");
	            }
	            if(trim($filtro['text']) != "" &&  $filtro['tipo'] == 'number' )
	            {
	                $query->where($name, '=' , $filtro['text']);
	            }

        	}
        }
    }

    public function cliente()
    {
        return $this->belongsTo('App\Cliente');
    }

    public function empresa()
    {
        return $this->belongsTo('App\Empresa');
    }

    public function estado()
    {
        return $this->belongsTo('App\Estado');
    }

}

