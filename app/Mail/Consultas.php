<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Consultas extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The consulta instance.
     *
     * @var Order
     */
    public $consulta;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($consulta)
    {
         $this->consulta = $consulta;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('consultas.mail');
    }
}
